FROM resin/rpi-raspbian

RUN apt-get update && apt-get install -y apache2
EXPOSE 80

ENTRYPOINT apachectl -e debug -DFOREGROUND

